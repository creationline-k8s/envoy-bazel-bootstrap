FROM ubuntu:18.04

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y ca-certificates \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /tmp/* /var/tmp/* \
    && rm -rf /var/lib/apt/lists/*

COPY out/envoy /usr/local/bin/envoy
RUN mkdir -p /etc/envoy
COPY google_com_proxy.v2.yaml /etc/envoy/config.yaml
EXPOSE 10000
CMD ["envoy", "-c", "/etc/envoy/config.yaml"]

envoy-bazel-bootstrap
=====================

Build envoy in ARM64 etc.

## Steps
- `docker build -f Dockerfile.build -t envoy-bazel-bootstrap .`
- `mkdir -p /tmp/envoy-bazel-bootstrap out`
- `docker run -v $PWD/out:/out -v /tmp/envoy-bazel-bootstrap/cache:/cache envoy-bazel-bootstrap`
- `docker build -t envoy .`

To test the image, run the image with port arg: `docker run -p 10000:10000 envoy`.
Then `curl localhost:10000 >/dev/null -v` and it should return HTTP 200.

## Notice

- You may need more than 10GB storage(total; include OS). We recommend you to prepare storage space more than 16GB.
